# README #

This is an test app by using Meteor for creating a demo Telegram BOT.


### What is this repository for? ###

* This is a sample meteor application.

### Requirements for development ###
* meteor

### How do I get set up? ###

If you don't have Meteor installed already, from a terminal window:


```
#!shell

curl install.meteor.com | /bin/sh

```
To run the app:

```
#!shell

# from ~/meteor-community-new
meteor
```

The app should now be running on http://localhost:3000.

### For Login into Application ###

You need to login for see the BOT activities by these credentials

username/email : admin@demo.com

password : demo

### What is the url or BOT name ###

you can access our BOT here : https://telegram.me/music_drum_bot