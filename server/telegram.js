var fs = Npm.require('fs');

Meteor.startup(function() {
    // set our token
    var Api = Meteor.npmRequire('teabot-telegram-api');
    var botApi = new Api(TOKEN);

    TelegramBot.token = TOKEN;
    TelegramBot.start(); // start the bot

    TelegramBot.addListener('/playdrum', function(command, username, original) {
        console.log("Command",command)
        var filePath = process.env.PWD + '/public/';
        if (!command[1]) {
            botApi.sendMessage(original.chat.id, 'Please select the one option like /playdrum [0-9]');
            updateDB('Send message: Please select the one option like /playdrum [0-9]');
        } else {
            switch (command[1]) {
                case "0":
                    filePath += "0.mp3";
                    sendAudio(0);
                    break;
                case "1":
                    filePath += "1.mp3";
                    sendAudio(1);
                    break;
                case "2":
                    filePath += "2.mp3";
                    sendAudio(2);
                    break;
                case "3":
                    filePath += "3.mp3";
                    sendAudio(3);
                    break;
                case "4":
                    filePath += "4.mp3";
                    sendAudio(4);
                    break;
                case "5":
                    filePath += "5.mp3";
                    sendAudio(5);
                    break;
                case "6":
                    filePath += "6.mp3";
                    sendAudio(6);
                    break;
                case "7":
                    filePath += "7.mp3";
                    sendAudio(7);
                    break;
                case "8":
                    filePath += "8.mp3";
                    sendAudio(8);
                    break;
                case "9":
                    filePath += "8.mp3";
                    sendAudio(9);
                    break;
                default:
                    var user = Command.findOne({
                        first_name: original.chat.first_name,
                        last_name: original.chat.last_name
                    })
                    if (user && user.selectedOption) {
                        if (command[1] == playdrum_stocks[user.selectedOption]) {
                            botApi.sendMessage(original.chat.id, 'correct Enjoy' + LINK);
                            updateDB('Send Message:correct Enjoy');
                        } else {
                            botApi.sendMessage(original.chat.id, 'incorrect');
                            updateDB('Send Message:incorrect' );
                        }
                    } else {
                        botApi.sendMessage(original.chat.id, 'You should select the one option like /playdrum [0-9]');
                        updateDB('Send Message: You should select the one option like /playdrum [0-9]');

                    }
            }

            function sendAudio(select) {
                var inputFile = {
                    buffer: fs.readFileSync(filePath),
                    fileName: filePath
                };
                botApi.sendAudio(original.chat.id, inputFile, {
                    duration: 30,   
                    reply_to_message_id: original.message_id,
                    // ReplyKeyboardMarkup: {keyboard : ["R","L"]}
                }).then(function(response) {
                    botApi.sendMessage(original.chat.id, 'Please play the sound and listen rhythm carefully and response drum rudiment, using the same order like RRRLLR by using this command /playdrum RRRLLR',{reply_to_message_id: original.message_id})
                });
                Command.upsert({
                    first_name: original.chat.first_name,
                    last_name: original.chat.last_name
                }, {
                    $set: {
                        first_name: original.chat.first_name,
                        last_name: original.chat.last_name,
                        selectedOption: select
                    }
                });
                var str = 'Send Audio: ' + select + '.mp3'
                updateDB(str);
            }

            function updateDB(res) {
                Command.upsert({
                    first_name: original.chat.first_name,
                    last_name: original.chat.last_name
                }, {
                    $set: {
                        first_name: original.chat.first_name,
                        last_name: original.chat.last_name,
                        chat_id: original.chat.id
                    },
                    $push: {
                        commands: {
                            command: command,
                            TS: new Date(),
                            res: {
                                message: res,
                                TS: new Date()
                            }
                        }
                    }
                })
            }

        }

    })
    TelegramBot.addListener('/help', function(command) {
        var msg = "I have the following commands loaded:\n";
        TelegramBot.triggers.text.forEach(function(post) {
            msg = msg + "- " + post.command + "\n";
        });
        return msg;
    })
    TelegramBot.addListener('/start', function(command, userName, original) {
        var msg = "Welcome " + original.chat.first_name;
        return msg;
    })
    
});
