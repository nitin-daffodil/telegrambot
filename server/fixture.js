fixture = {
    createUser: function() {
        id = Accounts.createUser({
            email: "admin@demo.com",
            password: "demo",
            username: 'admin',
            profile: {
                firstName: "Admin",
            }
        });
        Roles.addUsersToRoles(id, ['admin'])
    }
}


Meteor.startup(function() {
    if (Meteor.users.find({
            "roles": "admin"
        }).count() === 0) {
        fixture.createUser();
    }
})
