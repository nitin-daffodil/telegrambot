Template.telegram.helpers({
	isLoggedIn: function() {
		return Meteor.userId()
	}
})

Template.admin.helpers({
	commandList: function() {
		return Command.find({})
	},
	FormatCommand: function(command,index) {
		if(command) {
			return command[index];
		}
		return false
	}	  
})

UI.registerHelper('formatTs', function(context) {
    if (!context instanceof Date) {
        context = new Date(context)
    }
    return context.getFullYear() + '-' + ('00' + (context.getMonth() + 1)).substr(-2) + '-' + ('00' + context.getDate()).substr(-2) + ' ' + ('00' + context.getHours()).substr(-2) + ':' + ('00' + context.getMinutes()).substr(-2) + ':' + ('00' + context.getSeconds()).substr(-2);                      
})

Template.admin.onRendered(function(){
	this.autorun(function() {
        Meteor.userId() && Roles.userIsInRole(Meteor.userId(), ['admin'])  ? Meteor.subscribe('commands') : ''
    })
})